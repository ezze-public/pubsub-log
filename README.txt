
# installed : 
# apt -y install curl php-curl php-xml php-mongodb
# composer require mongodb/laravel-mongodb
# composer require predis/predis

# install mongo
mongo_pass=$(cat /etc/passwd | md5sum  | awk {'print $1'})
wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/on-docker/mongodb/README.txt | bash -s pubsub-log-mongo $mongo_pass

# install web
wget -qO- https://gitlab.com/ezze-public/conf/-/raw/main/on-docker/laravel/public/README.txt | bash -s pubsub-log 6767 - https://gitlab.com/ezze-public/pubsub-log/-/raw/main/conf/patch

# shell exec - mongo_pass -> .env
