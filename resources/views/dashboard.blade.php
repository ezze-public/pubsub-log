@extends('layouts.app')

@section('content')
<div class="container dashboard">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="card">

                <div class="card-body">

                    <form class="publish">
                        <input type="text" class="channel" placeholder="channel"/>
                        <input type="text" class="message" placeholder="message"/>
                        <input type="button" name="publish" value="publish"/>
                    </form>

                    <hr>

                    <form class="filter">

                        <input type="text" class="channel" placeholder="channel"/>
                        <input type="text" class="message" placeholder="message"/>
                        <input type="text" class="daterange" placeholder="date range"/>

                        <input type="button" name="filter" value="filter"/>

                        <br>
                        <input type="text" readonly=1 class="readonly-channel"/>
                        <input type="text" readonly=1 class="readonly-message"/>
                        <input type="text" readonly=1 class="readonly-daterange"/>
                        

                        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
                        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
                        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
                        <script>
                        $(function() {

                            // datepicker
                            $('input.daterange').daterangepicker({
                                timePicker: true,
                                autoUpdateInput: false,
                                locale: {
                                    format: 'YYYY/MM/DD HH:mm'
                                }
                            });
                            $('input.daterange').on('apply.daterangepicker', function(ev, picker) {
                                $(this).val(picker.startDate.format('YYYY/MM/DD HH:mm') + ' - ' + picker.endDate.format('YYYY/MM/DD HH:mm'));
                            });

                            // log view interval
                            setIntervalRepeat();
                            // sync_list();
                            
                            var old_last_id = null;
                            var the_last_id = null;
                            var ajax_token = null;
                            var interval_flag = true;

                            function setIntervalRepeat() {
                                if( interval_flag )
                                    sync_list();
                                setTimeout(setIntervalRepeat, 1000);
                            }

                            function sync_list( reset = false, page = 1 ){

                                if( page == 1 )
                                    interval_flag = true;
                                else
                                    interval_flag = false;
                                
                                if( reset == true ){
                                    $('list').html('');
                                    old_last_id = null;
                                }

                                ajax_token = Date.now();
                                data = { channel: $('form.filter .readonly-channel').val(), message: $('form.filter .readonly-message').val(), daterange: $('form.filter .readonly-daterange').val(), token: ajax_token };
                                // console.log(data);

                                $.ajax({
                                    url: '/api/message/list/'+page,
                                    type: 'GET',
                                    data: data,
                                    contentType: 'application/json',
                                    // dataType: 'json',

                                }).done(function(http) {

                                    // console.log('http');

                                    if( http.status != 'OK' ){
                                        console.log('status not OK');

                                    } else if( http.token != ajax_token ){
                                        console.log('late response');

                                    } else if( http.list.length == 0 ){
                                        console.log('empty list');
                                    
                                    } else {

                                        the_last_id = http.list[0]._id;
                                    
                                        if( old_last_id != null && the_last_id == old_last_id ){
                                            // console.log('same');

                                        } else {

                                            console.log('sync');

                                            if( $('list').html() == '' ){
                                                type_of_task = 'append'; // new list

                                                // fill pagination
                                                $('pagination').html('');
                                                for (let i = 1; i <= http.page[1]; i++) {
                                                    if( i == http.page[0] ){
                                                        $('pagination').append('<b>'+i+'</b>');
                                                    } else if( i == 1 || i == http.page[1] ){
                                                        $('pagination').append('<n>'+i+'</n>');                                                        
                                                    } else if( i == http.page[0] - 5 || i == http.page[0] + 10 ){
                                                        $('pagination').append(' .. ');
                                                    } else if( i < http.page[0] - 5 || i > http.page[0] + 10 ){
                                                        //
                                                    } else {
                                                        $('pagination').append('<n>'+i+'</n>');
                                                    }
                                                }

                                            } else {
                                                type_of_task = 'prepend'; // updating list
                                                $('list').prepend('<hr>');
                                            }

                                            http.list.every( item => {

                                                // console.log(item._id);

                                                if( item._id == old_last_id )
                                                    return false;

                                                item.date = new Date( parseInt(item.created_at.$date.$numberLong) ).toISOString().replace('T',' ').replace('-','/').split('.')[0];

                                                if( type_of_task == 'prepend' ){
                                                    $('list').prepend('<item><channel>' + item.channel + '</channel><message>' + item.message + '</message><date>' + item.date + '</date></item>');
                                                } else {
                                                    $('list').append('<item><channel>' + item.channel + '</channel><message>' + item.message + '</message><date>' + item.date + '</date></item>');
                                                }

                                                return true;

                                            });

                                            old_last_id = the_last_id;

                                        }

                                    }

                                });

                            }

                            // click on page number
                            $('pagination').delegate('n', 'click', function() {
                                $('pagination').html('');
                                var page = $(this).html();
                                console.log('the page is ' + page);                                
                                sync_list(reset=true, page);
                            });
                            
                            // sync list by click filter button
                            $('form.filter input[name=filter]').on('click', function(){
                                $('pagination').html('');
                                $('form.filter input.readonly-channel').val( $('form.filter input.channel').val() );
                                $('form.filter input.readonly-message').val( $('form.filter input.message').val() );
                                $('form.filter input.readonly-daterange').val( $('form.filter input.daterange').val() );
                                sync_list(reset=true);
                            });
                            
                            // publish button click
                            $('form.publish input[name=publish]').on('click', function(){
                                channel = $('form.publish .channel').val();
                                message = $('form.publish .message').val();
                                if( !channel || !message ){
                                    alert('no channel/message defined');
                                } else {
                                    $.ajax({
                                        url: '/api/pubsub/publish',
                                        type: 'POST',
                                        data: { channel: encodeURI(channel), message: encodeURI(message) },

                                    }).done(function(code) {
                                        if( code.status == 'OK' ){
                                            sync_list();
                                        } else {
                                            console.log(code);
                                        }
                                    });
                                }
                            });

                        });
                        </script>

                    </form>
                    <list></list>
                    <pagination></pagination>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
