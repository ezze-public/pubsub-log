<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\pubsubController;
use App\Http\Controllers\messageController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/


Route::get('/message/list/{page?}', [messageController::class, 'list']);
Route::post('/pubsub/publish', [pubsubController::class, 'publish']);

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
