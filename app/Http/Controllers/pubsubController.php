<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

class pubsubController extends Controller
{
    public static function publish(){

        $channel = urldecode($_POST['channel']);
        $message = urldecode($_POST['message']);

        Redis::publish($channel, $message);

        return response()->json([
            'status' => 'OK',
            'channel' => $channel,
            'message' => $message,
        ], 200);
        
    }
}
