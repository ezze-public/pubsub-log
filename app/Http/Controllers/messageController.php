<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Message;
use Carbon\Carbon;

class messageController extends Controller
{
    
    public static function list( int $page = 1 )
    {

        $r = Message::orderBy('created_at', 'desc');
        $r = $r->where('_id', '<>', '66624ae0d39763c013046413');

        if( array_key_exists('channel', $_GET) and $channel = trim($_GET['channel']) )
            $r = $r->where('channel', 'like', "%{$channel}%" );

            if( array_key_exists('message', $_GET) and $message = trim($_GET['message']) )
            $r = $r->where('message', 'like', "%{$message}%" );

        if( array_key_exists('daterange', $_GET) and $daterange = trim($_GET['daterange']) ){
            
            $daterange = str_replace('/', '-', $daterange);
            list($date_from, $date_to) = explode(' - ', $daterange);
            
            $date_from = new Carbon(strtotime($date_from));
            $date_to = new Carbon(strtotime($date_to));

            $r = $r->whereBetween('created_at', [$date_from, $date_to]);

        }

        # pagination
        $take = 14;
        $skip = ($page - 1) * $take;
        $c = $r->skip($skip)->take($take);

        $http = [ 
            'status' => 'OK',
            'page' => [ $page, ceil( $c->count() / $take ) ],
            'list' => $r->get(),
        ];

        if( array_key_exists('token', $_GET) )
            $http['token'] = $_GET['token'];
        
        return response()->json($http, 200);

    }

}
